
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;

namespace Diplomski 
{
	public interface IPublisher
	{
		void Publish(IInternal obj);
	}


	public class FilePublisher : IPublisher
	{
		private ILogger _log = LogManager.GetLogger("FilePublisher");
		private string _root;

		public FilePublisher(IConfigurationSection section)
		{
			_root = section["root"];
		}

		public FilePublisher(string root)
		{
			_root = root;
		}


		public void Publish(IInternal obj)
		{
			try
			{
				_log.Info("Publishing {0} - {1}",obj.Type,obj.Key);
				var filename = _root + "\\" + Utils.GetFileName(obj.Type, obj.Key);
				var json = JsonConvert.SerializeObject(obj,Formatting.Indented);
				File.Delete(filename);
				File.WriteAllText(filename,json);
				_log.Info("Succesfully published {0} - {1}",obj.Type,obj.Key);
			}
			catch (Exception ex)
			{
				_log.Error(ex,"on publishing {0}",obj);
				throw;
			}
		}

	}
}