namespace Diplomski
{
    public interface IInternalCreator 
    {
        IInternal Create(object obj);
    }

	public class InternalCreator : IInternalCreator
	{
		public IInternal Create(object afObject) 
		{
			var league = (afObject as AFLeague);
			if (league!=null)
				return new AFLeagueAdapter(league);

			var fixture = (afObject as AFFixture);
			if (fixture!=null)
				return new AFFixtureAdapter(fixture);

			var market = (afObject as AFMarket);
			if (market!=null)
				return new AFMarketAdapter(market, new AFMarketMapping());
			
			return null;
		}

	}


}