
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Diplomski 
{

	public class CachingPublisher : IPublisher
	{
		private IPublisher _basePublisher;
		private ICache _cache;
		private IFetcher _fetcher;

		private ILogger _log = LogManager.GetLogger("CachingPublisher");

		protected string Key(IInternal obj)
		{
			return obj.Type.ToString()+obj.Key;
		}

		public CachingPublisher(IPublisher basePublisher,IFetcher fetcher, ICache cache)
		{
			_basePublisher = basePublisher;
			_cache = cache;
			_fetcher = fetcher;
		}

		public void Publish(IInternal obj)
		{
			try
			{
				_log.Info("Publishing {0} - {1}",obj.Type,obj.Key);
				var fetched = _cache.Get(Key(obj));

				if (fetched==null)
				{
					fetched = _fetcher.Fetch(obj.Type, obj.Key);
					_cache.Put(GetKey(obj),fetched);
				}

				if (!obj.DeepEquals(fetched))
				{
					_basePublisher.Publish(obj);
					_cache.Put(GetKey(obj),obj);
					_log.Info("Succesfully published {0} - {1}",obj.Type,obj.Key);
				}
				else
				{
					_log.Info("No changes, already published {0} - {1}",obj.Type,obj.Key);
				}
				
			}
			catch (Exception ex)
			{
				_log.Error(ex,"publishing {0}",obj);
				throw;
			}
		}

		protected string GetKey(IInternal obj)
		{
			return obj.Type.ToString()+obj.Key;
		}
	}
}