using System.Collections.Generic;

namespace Diplomski
{
	public interface ICache
	{
		void Put(string key, object obj);

		object Get(string key);
	}

	public class LRUCache : ICache
	{
		protected Dictionary<string, object> _data = new Dictionary<string, object>();
		protected List<string> _usage = new List<string>();
		protected int _capacity;

		public LRUCache(int capacity)
		{
			_capacity = capacity;
		}

		public object Get(string key)
		{
			if (!_data.ContainsKey(key))
				return null;
			_usage.Remove(key);
			_usage.Insert(0, key);
			return _data[key];
		}

		public void Put(string key, object obj)
		{
			_data[key] = obj;
			if (_usage.Contains(key))
				_usage.Remove(key);
				_usage.Insert(0, key);
			if (_usage.Count > _capacity)
			{
				var last = _usage[_usage.Count - 1];
				_data.Remove(last);
				_usage.Remove(last);
			}
		}

		public int Count
		{
			get { return _data.Count; }
		}

		public IEnumerable<string> Keys
		{
			get { foreach (var k in _usage) yield return k; }
		}
	}
}