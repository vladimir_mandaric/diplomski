﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplomski
{
    public class Utils
    {

        public static string FileNamePatch(string f)
        {
            return f.Replace(":", "I").Replace("/", "#").Replace("\\", "$").Replace("|", "I");
        }

        public static string GetFileName(InternalType type, string key)
        {
            return FileNamePatch(type.ToString() + "_" + key);
        }

    }
}
