﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using NLog;

namespace Diplomski
{
	class Program
	{
		static ILogger _log = LogManager.GetLogger("MAIN");

		static void Main(string[] args)
		{
			var builder = new ConfigurationBuilder()
    			.SetBasePath(Directory.GetCurrentDirectory())
    			.AddJsonFile("appconfig.json");
			var configuration = builder.Build();
			LogManager.LoadConfiguration("nlog.config");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
			ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
			IReceiver receiver = new AFReceiver(configuration.GetSection("AFReceiver"));
			IPublisher publisher = new FilePublisher(configuration.GetSection("FilePublisher"));
			IFetcher fetcher = new FileFetcher(configuration.GetSection("FileFetcher"));			
			ICache cache = new LRUCache(Convert.ToInt32(configuration["cacheCapacity"]));
			IPublisher cachePublisher = new CachingPublisher(publisher, fetcher, cache);
			IInternalCreator creator = new InternalCreator();
			Action<object> OnReceive = o =>
			{
				try
				{
					var i = creator.Create(o);
					cachePublisher.Publish(i);  
				}  
				catch (Exception ex)
				{
					OnException(ex, "");
				}
			};
			while(Console.KeyAvailable)
				receiver.Receive(OnReceive, OnException);
		}


		static void OnException(Exception ex, string s)
		{
			_log.Error(ex,s);
		}
	}
}
