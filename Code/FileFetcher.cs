
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using NLog;
using Microsoft.Extensions.Configuration;

namespace Diplomski 
{
	public interface IFetcher
	{
		IInternal Fetch(InternalType type, string key);
	}


	public class FileFetcher : IFetcher
	{
		private string _root;
		private ILogger _log = LogManager.GetLogger("FileFetcher");

		public FileFetcher(IConfigurationSection section)
		{
			_root = section["root"];
		}

		public IInternal Fetch(InternalType type, string key)
		{
			try
			{
				_log.Info("Fetching {0} - {1}",type,key);
				var filename = _root+"\\"+Utils.GetFileName(type,key);
				if (!File.Exists(filename))
				{
					_log.Info($"No file to fetch {filename}");
					return null;
				}
				var json = File.ReadAllText(filename);
				IInternal obj = null;
				switch (type)
				{
					case InternalType.COMPETITION:
						obj = JsonConvert.DeserializeObject<Competition>(json);
						break;
					case InternalType.EVENT:
						obj = JsonConvert.DeserializeObject<Event>(json);
						break;
					case InternalType.MARKET:
						obj = JsonConvert.DeserializeObject<Market>(json, new JsonSerializerSettings { Converters = new List<JsonConverter> { new SelectionConverter() } });
						break;
					default:
						throw new FileLoadException($"on fetching {filename}");
				}
				_log.Info("Succesfully fetched {0} - {1}",type,key);
				return obj;
			}
			catch (Exception ex)
			{
				_log.Error(ex,"on fetching {0} - {1}",type,key);
				throw;
			}
		}
	}
}
