using System.Collections.Generic;
using Newtonsoft.Json;

namespace Diplomski 
{

	public partial class Api<T>
	{
		[JsonProperty("api")]
		public T ApiApi { get; set; }		
	}

	public partial class ApiLeagues
	{
		public Dictionary<string, AFLeague> Leagues { get; set; }
	}

	public partial class ApiFixtures
	{
		public Dictionary<string, AFFixture> Fixtures { get; set; }
	}

	public partial class ApiOdds
	{		
		public AFOdds Odds { get; set; }
	}
}