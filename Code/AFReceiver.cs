
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;

namespace Diplomski 
{
	public interface IReceiver
	{
		void Receive(Action<object> onReceive, Action<Exception, string> onException);
	}


	public class AFReceiver : IReceiver
	{
		protected ILogger _log = LogManager.GetLogger("AFReceiver");
		private readonly string _leaguesUrl;
		private readonly string _fixturesUrl;
		private readonly string _oddsUrl;
		private string FixturesUrl(string s) => string.Format(_fixturesUrl,s);
		private string OddsUrl(string s) =>  string.Format(_oddsUrl,s);

		private HttpClientHandler _clientHandler;
		private HttpClient _httpClient;

		public AFReceiver(IConfigurationSection section) 
		{
			_leaguesUrl = section["leaguesUrl"];
			_fixturesUrl = section["fixturesUrl"];
			_oddsUrl = section["oddsUrl"];

			_clientHandler = new HttpClientHandler {};
			_httpClient = new System.Net.Http.HttpClient(_clientHandler);			
		}

		public AFReceiver(string leaguesUrl, string fixturesUrl, string oddsUrl) 
		{
			_leaguesUrl = leaguesUrl;
			_fixturesUrl = fixturesUrl;
			_oddsUrl = oddsUrl;

			_clientHandler = new HttpClientHandler {};
			_httpClient = new System.Net.Http.HttpClient(_clientHandler);			
		}


		public void Receive(Action<object> onReceive, Action<Exception, string> onException) 
		{
			var leagues = TryGet<ApiLeagues>(_leaguesUrl, onException);
			if (leagues==null) return;

			foreach (var l in leagues.Leagues)
			{
				onReceive(l.Value);		
					  
				var fixtures = TryGet<ApiFixtures>(FixturesUrl(l.Key),onException);
				if (fixtures==null) continue;

				foreach (var f in fixtures.Fixtures) 
				{
					onReceive(f.Value);
					var fixtureId = f.Value.FixtureId;
					var odds = TryGet<ApiOdds>(OddsUrl(fixtureId.ToString()),onException);							
					if (odds==null) continue;
					foreach (var o in odds.Odds)
					{
						o.Value.FixtureId = (long)fixtureId;
						o.Value.MarketId = o.Key;
						onReceive(o.Value);
					}
				}
			}

		}


		protected T TryGet<T>(string url, Action<Exception, string> onException) 
		{
			T data = default(T);			 
			try
			{
				var str = _httpClient.GetStringAsync(url).Result;
				_log.Info($"from {url} received\r\n{str}");
				return  JsonConvert.DeserializeObject<Api<T>>(str).ApiApi;	
			}
			catch (Exception ex) 
			{
				_log.Error(ex,"on receiving {url}");
				return data;
			}
		}

		~AFReceiver()
		{
			_httpClient.Dispose();
			_clientHandler.Dispose();
		}
	}
}