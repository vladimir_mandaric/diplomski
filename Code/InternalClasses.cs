using System.Collections.Generic;
using System;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Diplomski 
{
	
	
	public class Competition : ICompetition
	{	
		public string CompetitionId { get; set; }
		public string Name { get; set; }
		public string SportId { get; set; }
		public InternalType Type => InternalType.COMPETITION;
		public string Key => CompetitionId;

		// override object.Equals
		public bool DeepEquals(object obj)
		{
			var obj1 = (obj as Competition);	
			if (obj1==null)
				return false;		
			return this.CompetitionId == obj1.CompetitionId 
				&& this.Name == obj1.Name
				&& this.SportId == obj1.SportId;
		}
		
	}

    
	public class Selection : ISelection
	{
		public string SelectionId { get; set; }
		public string MarketId { get; set; }
		public string Name { get; set; }
		public string ReferenceValue { get; set; }
		public string Price { get; set; }
		public string PriceTypeId { get; set; }
		public string SelectionStatusId { get; set; }
		public bool? IsWinning { get; set; }
		public InternalType Type => InternalType.SELECTION;
		public string Key => SelectionId;

		public bool DeepEquals(object obj)
		{
			var obj1 = (obj as Selection);
			if (obj1==null)
				return false;
				
			return this.SelectionId == obj1.SelectionId 
				&& this.Name == obj1.Name
				&& this.MarketId == obj1.MarketId
				&& this.ReferenceValue == obj1.ReferenceValue
				&& this.Price == obj1.Price
				&& this.PriceTypeId == obj1.PriceTypeId
				&& this.SelectionStatusId == obj1.SelectionStatusId
				&& this.IsWinning == obj1.IsWinning;
		}
		
	}
 
	public class Market : IMarket
	{
		
        public Market()
        {
            Selections = new List<ISelection>();
        }

		public string MarketId { get; set; }
		public string Name { get; set; }
		public string MarketTypeId { get; set; }
		public string PeriodId { get; set; }
		public string EventId { get; set; }
		public string MarketStatusId { get; set; }
		public List<ISelection> Selections { get; set; }
		public InternalType Type => InternalType.MARKET;
		public string Key => MarketId;

		
		public bool DeepEquals(object obj)
		{
			var obj1 = (obj as Market);
			if (obj1==null)
				return false;
			
			if (this.Selections.Count!=obj1.Selections.Count)
				return false;

			foreach (var s in this.Selections)
			{
				var s2 = obj1.Selections.Find( s1 => s1.Key == s.Key );
				if (!s.DeepEquals(s2))
					return false;
			}

			return this.Name == obj1.Name
				&& this.MarketId == obj1.MarketId
				&& this.MarketTypeId == obj1.MarketTypeId
				&& this.PeriodId == obj1.PeriodId
				&& this.EventId == obj1.EventId
				&& this.MarketStatusId == obj1.MarketStatusId;
		}
	}
 

	public class Team : ITeam
	{
		public string TeamId { get; set; }
		public string Name { get; set; }
		public InternalType Type => InternalType.TEAM;
		public string Key => TeamId;

		public bool DeepEquals(object obj)
		{
			
			var obj1 = (obj as ITeam);
			if (obj1==null)
				return false;
			

			return this.Name == obj1.Name
				&& this.TeamId == obj1.TeamId;

		}
	}


	public class Event : IEvent
   	{	
        public Event(Team homeTeam, Team awayTeam)
        {
            HomeTeam = homeTeam;
            AwayTeam = awayTeam;
        }

		public string EventId { get; set; }
		public string Name { get; set; }
		public string CompetitionId { get; set; }
		public string SportId { get; set; }
		public DateTime StartingTime { get; set; }

		public ITeam HomeTeam { get; set; }
		public ITeam AwayTeam { get; set; }
		public InternalType Type => InternalType.EVENT;
		public string Key => EventId;

		
		public bool DeepEquals(object obj)
		{
			var obj1 = (obj as IEvent);
			if (obj1==null)
				return false;
			

			return this.Name == obj1.Name
				&& this.EventId == obj1.EventId
				&& this.CompetitionId == obj1.CompetitionId
				&& this.SportId == obj1.SportId
				&& this.HomeTeam.DeepEquals(obj1.HomeTeam)
				&& this.AwayTeam.DeepEquals(obj1.AwayTeam)
				&& this.StartingTime==obj1.StartingTime;
		}
	}

    
    public class SelectionConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ISelection);
        }
        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }

        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var selection = default(ISelection);
            selection = new Selection();
            serializer.Populate(jsonObject.CreateReader(), selection);
            return selection;
        }
    }
}