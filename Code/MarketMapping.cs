    using System;

    namespace Diplomski
    {
        public class AFMarketMapping
        {

            public string GetType(string afMarketType) 
            {
                if (afMarketType=="Win the match") 
                    return "1x2";

                if (afMarketType.StartsWith("Handicap")) 
                    return "HCP";

                if (afMarketType=="Half Time") 
                    return "1x2";

                if (afMarketType=="Exact score") 
                    return "CRSC";

                if (afMarketType.StartsWith("Over / Under")) 
                    return "O/U";			
                
                throw new ArgumentException($"Can not determine market type for {afMarketType}");			
            }

            internal string GetPeriodId(string marketId)
            {
                if (marketId.Contains("Fist Half") || marketId.Contains("Half time")) 
                    return "1HLF";
                    
                if (marketId.Contains("Second Half")) 
                    return "2HLF";

                return "FG";
            }

        }

    }