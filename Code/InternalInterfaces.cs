using System;
using System.Collections.Generic;


namespace Diplomski 
{
	public enum InternalType 
	{
		COMPETITION,
		EVENT,
		MARKET,
        SELECTION,
        TEAM
    }

    public interface IInternal
	{
		InternalType Type { get; }

		string Key { get; }

		bool DeepEquals(object obj);
	}

    public interface ICompetition : IInternal
	{
		string CompetitionId { get; set; }
		string Name { get; set; }
		string SportId { get; set; }		
	}


	
	public interface IEvent : IInternal
   	{		
		string EventId { get; set; }
		string Name { get; set; }
		string CompetitionId { get; set; }
		string SportId { get; set; }
		DateTime StartingTime { get; set; }


		ITeam HomeTeam { get; set; }

		ITeam AwayTeam { get; set; }	
	}


    public interface ISelection : IInternal
	{
		string SelectionId { get; set; }
		string MarketId { get; set; }
		string Name { get; set; }
		string ReferenceValue { get; set; }
		string Price { get; set; }
		string PriceTypeId { get; set; }
		string SelectionStatusId { get; set; }
		bool? IsWinning { get; set; }
	}

	public interface IMarket : IInternal
	{

		string MarketId { get; set; }
		string Name { get; set; }
		string MarketTypeId { get; set; }
		string PeriodId { get; set; }
		string EventId { get; set; }
		string MarketStatusId { get; set; }
		List<ISelection> Selections { get; }
	}
	


	public interface ITeam : IInternal
	{
		string TeamId { get; set; }
		string Name { get; set; }
	}

}