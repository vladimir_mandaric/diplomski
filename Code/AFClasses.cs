using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Diplomski {

	public partial class AFLeague
	{
		[JsonProperty("league_id")]
		public long LeagueId { get; set; }
		public string Name { get; set; }
		public string Country { get; set; }
		public long Season { get; set; }
		
		[JsonProperty("season_start")]
		public DateTime SeasonStart { get; set; }

		[JsonProperty("season_end")]
		public DateTime SeasonEnd { get; set; }
		public Uri Logo { get; set; }
		public bool Standings { get; set; }
	}

	public partial class AFFixture
	{
		[JsonProperty("fixture_id")]
        public long FixtureId { get; set; }

	    [JsonProperty("event_timestamp")]
        public long EventTimestamp { get; set; }

        [JsonProperty("event_date")]
        public DateTime EventDate { get; set; }

        [JsonProperty("league_id")]
        public long LeagueId { get; set; }

        [JsonProperty("round")]
        public string Round { get; set; }

        [JsonProperty("homeTeam_id")]
        public long HomeTeamId { get; set; }

        [JsonProperty("awayTeam_id")]
        public long AwayTeamId { get; set; }

        [JsonProperty("homeTeam")]
        public string HomeTeam { get; set; }

        [JsonProperty("awayTeam")]
        public string AwayTeam { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("statusShort")]
        public string StatusShort { get; set; }

        [JsonProperty("goalsHomeTeam")]
        public long? GoalsHomeTeam { get; set; }

        [JsonProperty("goalsAwayTeam")]
        public long? GoalsAwayTeam { get; set; }

        [JsonProperty("halftime_score")]
        public string HalftimeScore { get; set; }

        [JsonProperty("final_score")]
        public string FinalScore { get; set; }

        [JsonProperty("penalty")]
        public object Penalty { get; set; }

        [JsonProperty("elapsed")]
        public long Elapsed { get; set; }

        [JsonProperty("firstHalfStart")]
        public long FirstHalfStart { get; set; }

        [JsonProperty("secondHalfStart")]
        public long SecondHalfStart { get; set; }
	}


	public partial class AFSelection
	{
		public string Label { get; set; }
		public long Pos { get; set; }
		public string Odd { get; set; }
	}


	[JsonDictionary]
	public partial class AFMarket : Dictionary<string, AFSelection>
	{
		public long FixtureId { get; set; }

		public string MarketId { get; set; }
	}


	[JsonDictionary]
	public partial class AFOdds : Dictionary<string, AFMarket>
	{   
	}

	public partial class AFTeam
	{
		public long TeamId { get; set; }
		public string Name { get; set; }
		public Uri Logo { get; set; }
	}

}