using System;
using System.Collections.Generic;

namespace Diplomski 

{
	
	public class AFLeagueAdapter : Competition
	{
		private static string _defaultSport = "FBL";

		public AFLeagueAdapter(AFLeague league) 
		{
			CompetitionId = league.LeagueId.ToString();
			Name = league.Name;
			SportId = _defaultSport;
		}
	}

	public class AFFixtureAdapter : Event
	{
		private static string _defaultSport = "FBL";

		

		private readonly List<ITeam> _teams = new List<ITeam>();
		
		public AFFixtureAdapter(AFFixture fixture) : base(null,null)
        {
			EventId = fixture.FixtureId.ToString();
			Name = $"{fixture.HomeTeam} - {fixture.AwayTeam}";
			CompetitionId = fixture.LeagueId.ToString();
			SportId = _defaultSport;
			StartingTime = fixture.EventDate;
			HomeTeam = new Team 
				{ 
					TeamId = fixture.HomeTeamId.ToString(),
					Name = fixture.HomeTeam
				};
			
			AwayTeam =  new Team 
				{ 
					TeamId = fixture.AwayTeamId.ToString(),
					Name = fixture.AwayTeam
				};
		}
	}

	public class AFMarketAdapter : Market
	{
		private static string _defaultStatusId = "OPEN";


		public AFMarketAdapter(AFMarket market, AFMarketMapping marketMapping) 
		{
			MarketId = $"{market.FixtureId}-{market.MarketId}";
			Name = market.MarketId;
			MarketTypeId = marketMapping.GetType(market.MarketId);
			PeriodId = marketMapping.GetPeriodId(market.MarketId);
			EventId = market.FixtureId.ToString();
			MarketStatusId = _defaultStatusId;
			foreach (var s in market) {
				Selections.Add(new AFSelectionAdapter(market,market.FixtureId,market.MarketId,s.Key,s.Value));
			}
		}
	}


	public class AFSelectionAdapter : Selection
	{   
		private static string _defaultSelectionStatusId = "OPEN";
		private static string _defaultPriceTypeId = "DEC";

		public AFSelectionAdapter(AFMarket market, long fixtureId, string marketId, string selectionId, AFSelection selection) 
		{
			MarketId = marketId;
			SelectionId = $"{fixtureId}-{marketId}-{selectionId}";
			Name = selection.Label;
			PriceTypeId = _defaultPriceTypeId;
			Price = selection.Odd;
			SelectionStatusId = _defaultSelectionStatusId;
		}

	}



}

